$(function() {
    var container = $('.related-blocks[data-tags-more]');

    container.each(function() {
        var elContainer = $(this),
            maxTag = elContainer.attr('data-tags-more'),
            allItem = elContainer.find('.related-wrap-link'),
            btnTag = $('<li class="btn-tag-more related-link"></li>');

        if (allItem.length > maxTag) {
            btnTag.appendTo(elContainer).addClass('storyzy-icon-tag-plus');

            allItem.each(function(index) {
                var el = $(this);
                if (index >= maxTag) {
                    el.addClass('sup-max-tag').hide();
                }
            });

            btnTag = elContainer.find(btnTag);
            var itemSupMax = elContainer.find('.sup-max-tag'),

                open = function(element) {
                    itemSupMax.show();
                    elContainer.find(btnTag).removeClass('storyzy-icon-tag-plus').addClass('storyzy-icon-tag-moins');
                },
                close = function(element) {
                    itemSupMax.hide();
                    btnTag.removeClass('storyzy-icon-tag-moins').addClass('storyzy-icon-tag-plus');
                };

            btnTag.click(function(el) {
                if (btnTag.hasClass('storyzy-icon-tag-plus')) {
                    open();
                } else {
                    close();

                }
            });
        }
    });
});